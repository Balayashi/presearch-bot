import webbrowser
import time
import os
import random
import sys
from tkinter import Tk
from tkinter.filedialog import askopenfilename


def testficheiro():
    try:
        Tk().withdraw() 
        filename = askopenfilename()  
        with open(filename, "r") as f: 
            linhas = f.read().splitlines()
            return linhas
    except FileNotFoundError:
        print("Colocar um ficheiro com frases.txt na mesma pasta")
        exit()


def processo(linhas):
    n = 1
    timer = 0
    while n > 0:
        new_line = random.choice(linhas)
        url = "https://presearch.org/extsearch?term={}".format(new_line)
        webbrowser.open(url)
        time.sleep(30)
        os.system("taskkill /im chrome.exe /f")
        print(url)
        timer+= 1
        print(timer)
        if timer == 32:
            time.sleep(86400)
            timer = 0
    
if __name__ == "__main__":
    totalinha = testficheiro()
    print(totalinha)
    processo(totalinha)